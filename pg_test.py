import psycopg2
from psycopg2 import sql

import tsdb.utils as dbu

with psycopg2.connect(user='postgres',
                      host='localhost',
                      dbname='tsdb',
                      password='postgres') as con:


    cent_table = dbu.get_column_names('centrifuge', con)
    cal_table = dbu.get_column_names('calibration', con)

    print(cent_table)
    print(cal_table)

    query = sql.SQL('SELECT * FROM centrifuge LIMIT 10')

    with con.cursor() as cursor:
        cursor.execute(query)
        result = cursor.fetchall()

        print(result)
